﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTesting {
    /// <summary>
    /// Class to hold and provide all FizzBuzz methods.
    /// </summary>
    public class FizzBuzzer {

        public FizzBuzzer() { }

        /// <summary>
        /// One implementation of a FizzBuzz method.
        /// </summary>
        /// <param name="number">The number to go to.</param>
        /// <returns></returns>
        public string FizzBuzz1(int number) {
            string ret = "";
            int counter = 0;

            while (counter < number) {
                counter++;
                string value;
                if (counter % 15 == 0) {
                    value = "FizzBuzz ";
                }
                else if (counter % 3 == 0) {
                    value = "Fizz ";
                }
                else if (counter % 5 == 0) {
                    value = "Buzz ";
                }
                else {
                    value = counter.ToString() + " ";
                }
                ret += value;
            }

            return ret;
        }

        /// <summary>
        /// Another implementation of a FizzBuzz method.
        /// </summary>
        /// <param name="number">The number to go to.</param>
        /// <returns></returns>
        public string FizzBuzz2(int number) {
            StringBuilder ret = new StringBuilder();
            int counter = 0;

            while (counter < number) {
                counter++;
                if (counter % 15 == 0) {
                    ret.Append("FizzBuzz ");
                }
                else if (counter % 3 == 0) {
                    ret.Append("Fizz ");
                }
                else if (counter % 5 == 0) {
                    ret.Append("Buzz ");
                }
                else {
                    ret.Append(counter.ToString() + " ");
                }
            }
            ret.Length--;
            return ret.ToString();
        }

        /// <summary>
        /// Another implementation of a FizzBuzz method.
        /// </summary>
        /// <param name="number">The number to go to.</param>
        /// <returns></returns>
        public string FizzBuzz3(int number) {
            int mid = number / 2;
            Task<StringBuilder> task1 = Task.Run(() => FizzBuzz3Processing(0, mid));
            Task<StringBuilder> task2 = Task.Run(() => FizzBuzz3Processing(mid++, number));

            return task1.Result.Append(task2.Result).ToString();
        }

        private StringBuilder FizzBuzz3Processing(int low, int high) {
            int counter = low;

            StringBuilder builder = new StringBuilder();
            while (counter < high) {
                counter++;
                if (counter % 15 == 0) {
                    builder.Append("FizzBuzz ");
                }
                else if (counter % 3 == 0) {
                    builder.Append("Fizz ");
                }
                else if (counter % 5 == 0) {
                    builder.Append("Buzz ");
                }
                else {
                    builder.Append(counter.ToString() + " ");
                }
            }

            return builder;
        }
    }
}
