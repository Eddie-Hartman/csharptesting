﻿using System;

namespace CSharpTesting {
    /// <summary>
    /// The purpose of this application is to practice unit testing and C# benchmarking.
    /// </summary>
    class Program {
        static void Main(string[] args) {
            FizzBuzzer fizzBuzzer = new FizzBuzzer();
            Console.WriteLine(fizzBuzzer.FizzBuzz1(15));
            Console.WriteLine(fizzBuzzer.FizzBuzz2(15));
        }
    }
}
