using NUnit.Framework;
using CSharpTesting;

namespace UnitTesting {
    /// <summary>
    /// This class holds the unit testing portion of FizzBuzz.
    /// </summary>
    public class Tests {

        private readonly string fizzBuzz15 = "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz";
        private readonly FizzBuzzer fizzBuzzer = new FizzBuzzer();

        /// <summary>
        /// In this case, we don't need a setup method.
        /// </summary>
        [SetUp]
        public void Setup() {
        }

        /// <summary>
        /// This test should fail due to the trailing space.
        /// </summary>
        /// <param name="number"></param>
        [Test]
        [TestCase(15)]
        public void Test1(int number) {
            string test = fizzBuzzer.FizzBuzz1(number);
            Assert.AreEqual(test, fizzBuzz15);
        }

        /// <summary>
        /// This test case should pass.
        /// </summary>
        /// <param name="number"></param>
        [Test]
        [TestCase(15)]
        public void Test2(int number) {
            string test = fizzBuzzer.FizzBuzz2(number);
            Assert.AreEqual(test, fizzBuzz15);
        }
    }
}