﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using CSharpTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Benchmarks {
    [MemoryDiagnoser]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class FizzBuzzBenchmark {
        private readonly FizzBuzzer fizzBuzzer = new FizzBuzzer();
        private readonly int NUMBER = 1000;

        [Benchmark]
        public void Benchmark1() {
            fizzBuzzer.FizzBuzz1(NUMBER);
        }

        [Benchmark(Baseline = true)]
        public void Benchmark2() {
            fizzBuzzer.FizzBuzz2(NUMBER);
        }

        [Benchmark]
        public void Benchmark3() {
            fizzBuzzer.FizzBuzz3(NUMBER);
        }
    }

    public class Config : ManualConfig {
        public Config() {
            AddExporter(CsvMeasurementsExporter.Default);
            AddExporter(RPlotExporter.Default);
        }
    }
}
