﻿using BenchmarkDotNet.Running;
using System;


namespace Benchmarks {
    class Program {
        static void Main(string[] args) {
            FizzBuzzBenchmark bench = new FizzBuzzBenchmark();
            var summary = BenchmarkRunner.Run<FizzBuzzBenchmark>();
        }
    }
}
